const log = idcUtils.getLogger('app')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const cookieSer = require('./lib/CookieSer')
const express = require('express')
const favicon = require('serve-favicon')
const morgan = require('morgan')
const path = require('path')
const compression = require('compression')
const jwtDecode = require('jwt-decode')

const app = express()

// router and libraries
const authService = require('./lib/AuthService')

<<<<<<< HEAD
const kamar = require('./routes/kamar')
=======
const dokter = require('./routes/dokter')

const pasien = require('./routes/pasien')
const dokterType = require('./routes/dokterType')
>>>>>>> 2537696f023772265f5e28ba11c0e5067722978c


// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')
app.set('x-powered-by', 'CaktiCore')

// resource middleware
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(express.static(path.join(__dirname, 'public'), {maxAge: 31557600}))
app.use(compression())

// access logger
// apache combined log format
app.use(morgan('combined', {stream: idcUtils.morganAccessLogStream}))

let appEnv = idcConfig.serviceEnv === 'prod' ? '' : idcConfig.serviceEnv
// process login and logout
// this should be put first so the request does not
// trapped into the 'catch-all' method below
app.get('/logout', function(req, res) {
  res.clearCookie('idc_fe_' + appEnv)
  res.redirect('/login?message=logout successful&isInfo=true')
})

app.get('/login', function(req, res) {
  if (req.cookies['idc_fe_' + appEnv]) {
    res.redirect('/')
  } else {
    res.render('login', {
      title: 'IDC ' + appEnv,
      authMessage: req.query.message,
      authMessageInfo: req.query.isInfo === 'true'
    })
  }
})

app.post('/login', function(req, res, next) {
  authService.authenticate(
    req.body.username,
    req.body.password,
    function(resp) {
      let decodedToken = jwtDecode(resp.access_token)

      res.cookie(
        'idc_fe_' + appEnv,
        cookieSer.ser({
          token: resp.access_token,
          username: decodedToken.uid,
          fullname: decodedToken.sub,
          id: decodedToken.uid
        }),
        {httpOnly: true, secure: idcConfig.server.scheme === 'HTTPS'}
      )
      res.redirect('/')
    },
    function(err) {
      log.debug(err)
      res.clearCookie('idc_fe_' + appEnv)
      if (typeof err.error_description === 'undefined')
        res.redirect('/login?message=Login failed, please try again&isInfo=false')

      res.redirect('/login?message=' + err.error_description + '&isInfo=false')
    }
  )
})

// 'catch-all' method
app.use(function(req, res, next) {
  log.info(`[${req.method}] ${req.originalUrl}`)
  // bypass auth check for static contents
  if (req.path.startsWith('/stylesheets') || req.path.startsWith('/javascripts')) {
    return next()
  }

  // check cookie validity
  if (req.cookies['idc_fe_' + appEnv]) {
    const cookie = cookieSer.dser(req.cookies['idc_fe_' + appEnv])
    if (cookie.token) {
      res.locals.token = cookie.token
      res.locals.userId = cookie.username
      res.locals.user = cookie.fullname
      res.locals.version = idcConfig.version
      res.locals.appEnv = appEnv
      return next()
    } else {
      res.clearCookie('idc_fe_' + appEnv)
      res.redirect('/login?message=User session has expired. Please login again&isInfo=false')
    }
  } else {
    // this handles redirect from /logout
    // do not check cookie if it does not exist
    res.redirect('/login')
  }
})

// root document handler
app.get('/', function(req, res) {
  if (!res.locals.user) {
    res.redirect('/login')
  } else {
    res.render('index', {
      title: 'IDC',
      user: res.locals.user,
      version: idcConfig.version,
      appEnv: appEnv
    })
  }
})


<<<<<<< HEAD
app.use('/kamar', kamar)
=======

app.use('/pasien', pasien)


app.use('/dokter', dokter)
app.use('/dokterType', dokterType)
>>>>>>> 2537696f023772265f5e28ba11c0e5067722978c


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.statusCode = 404
  next({status: 404, message: 'Page Not Found: ' + req.url})
})

// generic error handling and terminate all unhandled request
app.use(function(err, req, res, next) {
  // construct error object
  let error = err
  if (err.response) {
    error.status = res.statusCode = err.response.status
    error.message = err.response.statusText
    error.description = err.response.data.message
      ? err.response.data.message +
        ': ' +
        (err.response.data.description ? err.response.data.description : JSON.stringify(err.response.data.objects))
      : err.response.data
  } else if (err.syscall) {
    error.stack = err.syscall + ' ' + err.code + ' ' + err.address + ':' + err.port
  }
  if (!err.status) {
    error.status = res.statusCode = 500
    error.message = error.message || 'Internal Server Error'
  }
  error.path = req.path
  error.method = req.method
  error.data = req.body
  error.isAjax = req.xhr
  

  // log to output/logfile
  log.error(error)

  // send json or render error page
  if (req.xhr) {
    res.status(err.status).send({
      status: err.status,
      message: err.message,
      description: err.description
    })
  } else {
    res.statusCode = err.status
    res.render('error', {
      user: res.locals.user,
      version: idcConfig.version,
      status: err.status,
      message: err.message + (err.description ? ': ' + err.description : '')
    })
  }

  next()
})

process.on('uncaughtException', function(err) {
  log.error(err)
})

module.exports = app

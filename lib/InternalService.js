const services = require('./WebService')
const idc = services.idc
const log = idcUtils.getLogger('lib.InternalService')

//API Common
exports.listNodes = function(data, res, next, callback, fallout) {
  log.info(`[POST] /internal/node/list - data: ${JSON.stringify(data)}`)
  return idc
    .post('/internal/node/list', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (err.response)
        log.error('[ERROR] /internal/node/list - [%s:%s] %s\ndata = %o', err.response.status, err.response.statusText, err.message, data)
      else 
        log.error('[ERROR] /internal/node/list - [%s]\ndata = %o', err.message, data)
      if (fallout) {
        fallout(err)
      } else {
        if (err.response.status === 404) {
          callback({nodes: []})
        } else {
          next(err)}
      }
    })
}

exports.createNode = function(data, res, next, callback, fallout) {
  log.debug('[POST] /internal/node')
  log.debug(data)
  return idc
    .post('/internal/node', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.cypher = function(data, res, next, callback, fallout) {
  log.info('[POST] /internal/cypher')
  log.info(data)
  return idc
    .post('/internal/cypher', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.getNode = function(nodeId, includeRelation, res, next, callback, fallout) {
  log.debug(`[GET] /internal/node?id=${nodeId}&includeRelations=${includeRelation}`)
  return idc
    .get(`/internal/node?id=${nodeId}&includeRelations=${includeRelation}`, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.getNodesByLabelPath = function(nodeId, labels, res, next, callback, fallout) {
  log.debug(`[GET] /internal/nodesByLabelPath?id=${nodeId}&labels=${labels}`)
  return idc
    .get(`/internal/nodesByLabelPath?id=${nodeId}&labels=${labels}`, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.getNodeSelectedRelationship = function(nodeId, relationshipTypes, res, next, callback, fallout) {
  log.debug(`[GET] /internal/node?id=${nodeId}&includeRelations=true&relationshipTypes=${relationshipTypes}`)
  return idc
    .get(`/internal/node?id=${nodeId}&includeRelations=true&relationshipTypes=${relationshipTypes}`, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.updateNode = function(data, res, next, callback, fallout) {
  log.debug('[PUT] /internal/node')
  log.debug(data)
  return idc
    .put('/internal/node', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.deleteNode = function(nodeId, res, next, callback, fallout) {
  log.debug(`[DELETE] /internal/node?id=${nodeId}`)
  return idc
    .delete(`/internal/node?id=${nodeId}`, {}, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.deleteNodeWithEdges = function(data, res, next, callback, fallout) {
  log.debug('[DELETE] /internal/nodeWithRelations')
  log.debug(data)
  return idc
    .delete('/internal/nodeWithRelations', {data: data, headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.createRelationship = function(data, res, next, callback, fallout) {
  log.debug('[POST] /internal/relationship')
  log.debug(data)
  return idc
    .post('/internal/relationship', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.deleteRelationship = function(relationshipId, res, next, callback, fallout) {
  log.debug(`[DELETE] /internal/relationship?id=${relationshipId}`)
  return idc
    .delete(`/internal/relationship?id=${relationshipId}`, {}, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.deleteNodeProperties = function(nodeId, properties, res, next, callback, fallout) {
  let url = `/internal/node/property?id=${nodeId}`
  log.debug(`[DELETE] ${url}`)
  
  return idc
    .delete(url, {
      data: properties,
      headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.moveRelationship = function(data, res, next, callback, fallout) {
  let url = `/internal/relationship/move?id=${data.id}`
  if (data.endNodeId) url += `&endNodeId=${data.endNodeId}`
  if (data.startNodeId) url += `&startNodeId=${data.startNodeId}`

  log.debug(`[PUT] ${url}`)

  return idc
    .put(url, data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}
exports.detachDelete = function(_id, res, next, callback, fallout) {
  let data = {
    query: "match (n) where id(n)=toInt($id) detach delete n",
    params: {
      id:_id
    }
  }
  
  return idc
    .post('/internal/cypher', data, {
      headers: { 'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token }
    })
    .then(function (resp) {
      callback(resp.data)
    })
    .catch(function (err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}
exports.updateRelationship = function(data, res, next, callback, fallout) {
  log.debug('[PUT] /internal/relationship')
  log.debug(data)
  return idc
    .put('/internal/relationship', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}
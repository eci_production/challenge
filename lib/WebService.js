const axios = require('axios')
const log = idcUtils.getLogger('lib.WebService')

log.info('idc: ' + idcConfig.service.idc.baseURL)

exports.idc = axios.create(idcConfig.service.idc)

exports.idm = axios.create(idcConfig.service.idm)
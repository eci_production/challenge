const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'nip', label: 'Nip', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'nama', label: 'Nama Dokter', filter: {type: 'text'}},
  {name: 'ttl', label: 'Tanggal Lahir', filter: {type: 'date'}},
  {name: 'jabatan', label: 'jabatan', filter: {type: 'text'}},
  {name: 'alamat', label: 'Alamat', filter: {type: 'text'}},
  {name: 'status', label: 'Status', type: 'boolean'},
  {name: 'createdAt', label: 'Created At', type: 'date'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Dokter',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.nip] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.nip) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.nip] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          nip: item.node.nip ? item.node.nip : null,
          nama: item.node.nama ? item.node.nama : null,
          ttl: item.node.ttl ? item.node.ttl : null,
          jabatan: item.node.jabatan ? item.node.jabatan : null,
          alamat: item.node.alamat ? item.node.alamat : null,
          status: item.node.status ? item.node.status : null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/dokter/' + item.node._id, method: 'GET'}
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('dokter/list', {
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/dokter/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  const reqData = {
    label: 'Dokter',
    properties: {
      nip: req.body.nip,
      nama: req.body.nama,
      ttl: req.body.ttl,
      jabatan: req.body.jabatan,
      alamat: req.body.alamat,
      status: 'ACTIVE'
    }
  }

  internalService.createNode(reqData, res, next, function(resData) {
    res.status(200).send({
      links: {
        view: {url: '/dokter/' + resData.node._id, method: 'GET'}
      }
    })
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Dokter',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/dokter/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []

    var item = resData
    data.push({
      _id: item.node._id ? item.node._id : null,
      nip: item.node.nip ? item.node.nip : null,
      nama: item.node.nama ? item.node.nama : null,
      ttl: item.node.ttl ? item.node.ttl : null,
      jabatan: item.node.jabatan ? item.node.jabatan : null,
      alamat: item.node.alamat ? item.node.alamat : null,
      status: item.node.status ? item.node.status : null,
      createdAt: item.node.createdAt ? item.node.createdAt : null,
      createdBy: item.node.createdBy ? item.node.createdBy : null,
      updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
      updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
      links: {
        delete: {url: '/dokter/deleteNode/' + item.node._id, method: 'DELETE'},
        edit: {url: '/dokter/' + item.node._id, method: 'PUT'},
        addDokterType: {url: '/dokter/addDokterType/', method: 'POST'},
      },
      dokterType: []
    })

    item.relationships.forEach(function(itemDokterType){
      data[0].DokterType.push({
        _id: itemDokterType.node._id ? itemDokterType.node._id : null,
        nip: itemDokterType.node.nip ? itemDokterType.node.nip : null,
        nama: itemDokterType.node.nama ? itemDokterType.node.nama : null,
        ttl: itemDokterType.node.ttl ? itemDokterType.node.ttl : null,
        jabatan: itemDokterType.node.jabatan ? itemDokterType.node.jabatan : null,
        alamat: itemDokterType.node.alamat ? itemDokterType.node.alamat : null,
        status: itemDokterType.node.status ? itemDokterType.node.status : null,
        createdAt: itemDokterType.node.createdAt ? itemDokterType.node.createdAt : null,
        createdBy: itemDokterType.node.createdBy ? itemDokterType.node.createdBy : null,
        updatedAt: itemDokterType.node.updatedAt ? itemDokterType.node.updatedAt : null,
        updatedBy: itemDokterType.node.updatedBy ? itemDokterType.node.updatedBy : null,
        links: {
          view: {url: '/dokterType/' + itemDokterType.node._id, method: 'DELETE'},
          delete: {url: '/dokter/dokterType/' + item.node._id + '/' + itemDokterType.node._id, method: 'DELETE'},
        }
      })
    })
      
    res.render('dokter/details', {
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/deleteNode/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.detachDelete(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/dokter/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router

const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'nip', label: 'Nip', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'nama', label: 'Nama Lengkap Dokter', filter: {type: 'text'}},
  {name: 'j_sakit', label: 'Jenis Penyakit', filter: {type: 'text'}},
  {name: 'alamat', label: 'Alamat Dokter', filter: {type: 'text'}},
  {name: 'status', label: 'Status', type: 'boolean'},
  {name: 'createdAt', label: 'Created At', type: 'date'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'DokterType',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          nip: item.node.nip ? item.node.nip : null,
          nama: item.node.nama ? item.node.nama : null,
          j_sakit: item.node.j_sakit ? item.node.j_sakit : null,
          alamat: item.node.alamat ? item.node.alamat : null,
          status: item.node.status ? item.node.status : null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/dokterType/' + item.node._id, method: 'GET'}
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('dokterType/list', {
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/dokterType/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  const reqData = {
    label: 'DokterType',
    properties: {
      nip: req.body.nip,
      nama: req.body.nama,
      j_sakit: req.body.j_sakit,
      alamat: req.body.alamat,

      status: 'ACTIVE'
    }
  }

  internalService.createNode(reqData, res, next, function(resData) {

    if(req.body.dokterId){
      const relationData = {
        startNodeId: resData.node._id,
        endNodeId: req.body.dokterId,
        type: 'HAS_PARENT',
      }
  
      internalService.createRelationship(relationData, res, next, function(){
        res.status(200).send({
          links: {
            view: {url: '/dokterType/' + resData.node._id, method: 'GET'}
          }
        })
      })
    }else{
      res.status(200).send({
        links: {
          view: {url: '/dokterType/' + resData.node._id, method: 'GET'}
        }
      })
    }
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'DokterType',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/dokterType/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []
    var item = resData

    data.push({
      nip: item.node.nip ? item.node.nip : null,
      nama: item.node.nama ? item.node.nama : null,
      j_sakit: item.node.j_sakit ? item.node.j_sakit : null,
      alamat: item.node.alamat ? item.node.alamat : null,
      status: item.node.status ? item.node.status : null,
      createdAt: item.node.createdAt ? item.node.createdAt : null,
      createdBy: item.node.createdBy ? item.node.createdBy : null,
      updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
      updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
      links: {
        delete: {url: '/dokterType/deleteNode/' + item.node._id, method: 'DELETE'},
        edit: {url: '/dokterType/' + item.node._id, method: 'PUT'}
      }
    })
      
    res.render('dokterType/details', {
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/deleteNode/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.detachDelete(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/dokterType/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router

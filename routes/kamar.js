const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
<<<<<<< HEAD:routes/kamar.js
  {name: 'noKamar', label: 'No Kamar', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'namaKamar', label: 'Nama Kamar', filter: {type: 'text'}},
  {name: 'kelas', label: 'Kelas', filter: {type: 'text'}},
  {name: 'status', label: 'Status', type: 'boolean'},
=======
  {name: 'nik', label: 'Nik', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'nama', label: 'Nama pasien', filter: {type: 'text'}},
  {name: 'alamat', label: 'Alamat', filter: {type: 'text'}},
  {name: 'umur', label: 'Umur', filter: {type: 'text'}},
  {name: 'notelp', label: 'telepon', filter: {type: 'text'}},
  {name: 'jkel', label: 'kelamin', type: 'text'},
>>>>>>> 2537696f023772265f5e28ba11c0e5067722978c:routes/pasien.js
  {name: 'createdAt', label: 'Created At', type: 'date'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Kamar',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.nip] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.nip) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.nip] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
<<<<<<< HEAD:routes/kamar.js
          noKamar: item.node.noKamar ? item.node.noKamar : null,
          namaKamar: item.node.namaKamar ? item.node.namaKamar : null,
          kelas: item.node.kelas ? item.node.kelas : null,
          status: item.node.status ? item.node.status : null,
=======
          nik: item.node.nik ? item.node.nik : null,
          nama: item.node.nama ? item.node.nama : null,
          alamat: item.node.alamat ? item.node.alamat : null,
          umur: item.node.umur ? item.node.umur : null,
          notelp: item.node.notelp ? item.node.notelp : null,
          jkel: item.node.jkel ? item.node.jkel : null,
>>>>>>> 2537696f023772265f5e28ba11c0e5067722978c:routes/pasien.js
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/kamar/' + item.node._id, method: 'GET'}
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('kamar/list', {
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/kamar/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  const reqData = {
    label: 'Kamar',
    properties: {
<<<<<<< HEAD:routes/kamar.js
      noKamar: req.body.noKamar,
      namaKamar: req.body.namaKamar,
      kelas: req.body.kelas,
      status: 'ACTIVE'
=======
      nik: req.body.nik,
      nama: req.body.nama,
      alamat: req.body.alamat,
      umur: req.body.umur,
      notelp: req.body.notelp,
      jkel: req.body.jkel,
>>>>>>> 2537696f023772265f5e28ba11c0e5067722978c:routes/pasien.js
    }
  }

  internalService.createNode(reqData, res, next, function(resData) {
    res.status(200).send({
      links: {
        view: {url: '/kamar/' + resData.node._id, method: 'GET'}
      }
    })
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Kamar',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/kamar/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []

    var item = resData
    data.push({
      _id: item.node._id ? item.node._id : null,
<<<<<<< HEAD:routes/kamar.js
      noKamar: item.node.noKamar ? item.node.noKamar : null,
      namaKamar: item.node.namaKamar ? item.node.namaKamar : null,
      kelas: item.node.kelas ? item.node.kelas : null,
      status: item.node.status ? item.node.status : null,
=======
      nik: item.node.nik ? item.node.nik : null,
      nama: item.node.nama ? item.node.nama : null,
      alamat: item.node.alamat ? item.node.alamat : null,
      umur: item.node.umur ? item.node.umur : null,
      notelp: item.node.notelp ? item.node.notelp : null,
      jkel: item.node.jkel ? item.node.jkel : null,
>>>>>>> 2537696f023772265f5e28ba11c0e5067722978c:routes/pasien.js
      createdAt: item.node.createdAt ? item.node.createdAt : null,
      createdBy: item.node.createdBy ? item.node.createdBy : null,
      updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
      updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
      links: {
        delete: {url: '/kamar/deleteNode/' + item.node._id, method: 'DELETE'},
        edit: {url: '/kamar/' + item.node._id, method: 'PUT'},
        addKamarType: {url: '/kamar/addKamarType/', method: 'POST'},
      },
      pasienType: []
    })

    item.relationships.forEach(function(itemKamarType){
      data[0].pasienType.push({
<<<<<<< HEAD:routes/kamar.js
        _id: itemKamarType.node._id ? itemKamarType.node._id : null,
        noKamar: item.node.noKamar ? item.node.noKamar : null,
        namaKamar: item.node.namaKamar ? item.node.namaKamar : null,
        kelas: item.node.kelas ? item.node.kelas : null,
        status: itemKamarType.node.status ? itemKamarType.node.status : null,
        createdAt: itemKamarType.node.createdAt ? itemKamarType.node.createdAt : null,
        createdBy: itemKamarType.node.createdBy ? itemKamarType.node.createdBy : null,
        updatedAt: itemKamarType.node.updatedAt ? itemKamarType.node.updatedAt : null,
        updatedBy: itemKamarType.node.updatedBy ? itemKamarType.node.updatedBy : null,
=======
        _id: itemPasienType.node._id ? itemPasienType.node._id : null,
        nik: itemPasienType.node.nik ? itemPasienType.node.nik : null,
        nama: itemPasienType.node.nama ? itemPasienType.node.nama : null,
        alamat: itemPasienType.node.alamat ? itemPasienType.node.alamat : null,
        umur: item.node.umur ? item.node.umur : null,
        notelp: item.node.notelp ? item.node.notelp : null,
        jkel: item.node.jkel ? item.node.jkel : null,
        createdAt: itemPasienType.node.createdAt ? itemPasienType.node.createdAt : null,
        createdBy: itemPasienType.node.createdBy ? itemPasienType.node.createdBy : null,
        updatedAt: itemPasienType.node.updatedAt ? itemPasienType.node.updatedAt : null,
        updatedBy: itemPasienType.node.updatedBy ? itemPasienType.node.updatedBy : null,
>>>>>>> 2537696f023772265f5e28ba11c0e5067722978c:routes/pasien.js
        links: {
          view: {url: '/pasienType/' + itemKamarType.node._id, method: 'DELETE'},
          delete: {url: '/kamar/pasienType/' + item.node._id + '/' + itemKamarType.node._id, method: 'DELETE'},
        }
      })
    })
      
    res.render('kamar/details', {
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/deleteNode/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.detachDelete(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/kamar/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router

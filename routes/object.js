const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'name', label: 'Name', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'nameAlias', label: 'Name Alias', filter: {type: 'text'}},
  {name: 'description', label: 'Description', filter: {type: 'text'}},
  {name: 'status', label: 'Status', type: 'boolean'},
  {name: 'createdAt', label: 'Created At', type: 'date'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Object',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
          nameAlias: item.node.nameAlias ? item.node.nameAlias : null,
          description: item.node.description ? item.node.description : null,
          status: item.node.status ? item.node.status : null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/object/' + item.node._id, method: 'GET'}
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('object/list', {
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/object/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  const reqData = {
    label: 'Object',
    properties: {
      name: req.body.name,
      nameAlias: req.body.nameAlias,
      description: req.body.description,
      status: 'ACTIVE'
    }
  }

  internalService.createNode(reqData, res, next, function(resData) {
    res.status(200).send({
      links: {
        view: {url: '/object/' + resData.node._id, method: 'GET'}
      }
    })
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Object',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/object/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []

    var item = resData
    data.push({
      _id: item.node._id ? item.node._id : null,
      name: item.node.name ? item.node.name : null,
      nameAlias: item.node.nameAlias ? item.node.nameAlias : null,
      description: item.node.description ? item.node.description : null,
      status: item.node.status ? item.node.status : null,
      createdAt: item.node.createdAt ? item.node.createdAt : null,
      createdBy: item.node.createdBy ? item.node.createdBy : null,
      updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
      updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
      links: {
        delete: {url: '/object/detachDelete/' + item.node._id, method: 'DELETE'},
        edit: {url: '/object/' + item.node._id, method: 'PUT'},
        addObjectType: {url: '/object/addObjectType/', method: 'POST'},
      },
      objectType: []
    })

    item.relationships.forEach(function(itemObjectType){
      data[0].objectType.push({
        _id: itemObjectType.node._id ? itemObjectType.node._id : null,
        name: itemObjectType.node.name ? itemObjectType.node.name : null,
        nameAlias: itemObjectType.node.nameAlias ? itemObjectType.node.nameAlias : null,
        description: itemObjectType.node.description ? itemObjectType.node.description : null,
        status: itemObjectType.node.status ? itemObjectType.node.status : null,
        createdAt: itemObjectType.node.createdAt ? itemObjectType.node.createdAt : null,
        createdBy: itemObjectType.node.createdBy ? itemObjectType.node.createdBy : null,
        updatedAt: itemObjectType.node.updatedAt ? itemObjectType.node.updatedAt : null,
        updatedBy: itemObjectType.node.updatedBy ? itemObjectType.node.updatedBy : null,
        links: {
          view: {url: '/objectType/' + itemObjectType.node._id, method: 'DELETE'},
          delete: {url: '/object/objectType/' + item.node._id + '/' + itemObjectType.node._id, method: 'DELETE'},
        }
      })
    })
      
    res.render('object/details', {
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/detachDelete/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.detachDelete(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/object/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
